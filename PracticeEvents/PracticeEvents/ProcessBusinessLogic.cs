﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeEvents
{
    //Declare an Event need two step:
    //1. Declare a delegate
    //2. Declare a variable of the delegate with evnet keyword.

    class ProcessBusinessLogic
    {
        public event EventHandler<ProcessEventArgs> ProcessCompleted;

        public void StartProcess()
        {
            var data = new ProcessEventArgs();

            try
            {
                Console.WriteLine( "Process Started!" );

                data.IsSuccessful = true;
                data.CompletionTime = DateTime.Now;
                OnProcessCompleted( data );
            }
            catch ( Exception e )
            {
                data.IsSuccessful = false;
                data.CompletionTime = DateTime.Now;
                OnProcessCompleted( data );
            }
        }

        protected virtual void OnProcessCompleted( ProcessEventArgs e )
        {
            ProcessCompleted?.Invoke( this, e );
        }
    }
}
