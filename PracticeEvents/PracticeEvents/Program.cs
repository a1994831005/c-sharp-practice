﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeEvents
{
    class Program
    {

        static void Main( string[] args )
        {
            ProcessBusinessLogic b1 = new ProcessBusinessLogic();
            b1.ProcessCompleted += b1_ProcessCompleted;
            b1.StartProcess();
        }

        // event handler
        public static void b1_ProcessCompleted( object sender, ProcessEventArgs e )
        {
            Console.WriteLine( $"Process {( e.IsSuccessful ? "Completed Successfully" : "failed" )}" );
            Console.WriteLine( $"Completion Time: {e.CompletionTime.ToLongDateString()}" );
        }
    }
}
